# FadingTableView

### CocoaPods
```ruby
pod 'FadingTableView'
```

![Alt Text](https://bitbucket.org/theijligers/fadingtableview/raw/7992fb693e310529097b510035c93d3445638034/ReadmeMedia/FadingTableView.gif)

### Swift
```swift
let tableView = FadingTableView(frame: .zero)

let fadingConfiguration = FadingTableViewConfiguration(opaqueFadeDistance: 5,
                                                     gradientFadeDistance: 30,
                                                                fadeColor: .white)

tableView.topConfiguration = fadingConfiguration
tableView.bottomConfiguration = fadingConfiguration
tableView.animationDuration = 0.25
```
### Objective-C
```objc
_tableView = [[FadingTableView alloc] initWithFrame:CGRectZero];

FadingTableViewConfiguration *fadingConfiguration = [[FadingTableViewConfiguration alloc] initWithOpaqueFadeDistance:5
                                                                                          gradientFadeDistance:30
                                                                                          fadeColor:[UIColor whiteColor]];

[_tableView setTopConfiguration:configuration];
[_tableView setBottomConfiguration:configuration];
[_tableView setAnimationDuration:0.25];
```
