Pod::Spec.new do |s|
  s.name         = "FadingTableView"
  s.version      = "0.0.1"
  s.summary      = "A subclass of UITableView that can fade out content when scrolling."

  s.homepage = "https://bitbucket.org/theijligers/fadingtableview"

  s.license = { :type => 'MIT', :file => 'LICENSE' }

  s.author             = { "Ton Heijligers" => "tonheijligers@gmail.com" }
  s.source             = { :git => "https://bitbucket.org/theijligers/fadingtableview.git", :tag => '0.0.1' }

  s.source_files  = "FadingTableView/**/*.swift"
  
  s.ios.deployment_target  = '9.0'
  s.swift_version = '4.1'
end
