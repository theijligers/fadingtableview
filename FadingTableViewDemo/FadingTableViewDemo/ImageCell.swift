import UIKit

internal final class ImageCell: UITableViewCell {
    internal static let cellIdentifier: String = "ImageCell"

    private let sidePadding: CGFloat = 30

    override internal init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        imageView?.contentMode = .scaleAspectFit
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override internal func layoutSubviews() {
        super.layoutSubviews()

        imageView?.frame = contentView.bounds.insetBy(dx: sidePadding, dy: 0)
    }

    override internal func systemLayoutSizeFitting(_ targetSize: CGSize,
                                                   withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority,
                                                   verticalFittingPriority: UILayoutPriority) -> CGSize {
        setNeedsLayout()
        layoutIfNeeded()

        imageView?.sizeToFit()
        guard let size = imageView?.frame.size else {
            return targetSize
        }

        let aspectRatio = size.width / size.height
        let preferredHeight = ceil((contentView.frame.width - 2 * sidePadding) / aspectRatio)

        return CGSize(width: targetSize.width, height: preferredHeight)
    }
}
