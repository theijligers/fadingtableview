#import "ViewControllerObjC.h"

@import FadingTableView;

static NSString *const cellIdentifier = @"DefaultCell";

@interface ViewControllerObjC () <UITableViewDataSource> {
    FadingTableView *_tableView;
}

@end

@implementation ViewControllerObjC

- (void)loadView {
    [super loadView];
    
    _tableView = [[FadingTableView alloc] initWithFrame:CGRectZero];
    
    [_tableView setFrame:[[self view] bounds]];
    [_tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
    [_tableView setDataSource:self];
    
    FadingTableViewConfiguration *fadingConfiguration = [[FadingTableViewConfiguration alloc] initWithOpaqueFadeDistance:5
                                                                                              gradientFadeDistance:30
                                                                                              fadeColor:[UIColor whiteColor]];
    
    [_tableView setTopConfiguration:fadingConfiguration];
    [_tableView setBottomConfiguration:fadingConfiguration];
    [_tableView setAnimationDuration:0.25];
    
    [[self view] addSubview:_tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return section * 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [cell setBackgroundColor:[UIColor blackColor]];
    [[cell textLabel] setTextColor:[UIColor whiteColor]];
    [[cell textLabel] setText:[NSString stringWithFormat:@"%li - %li", [indexPath section], [indexPath row]]];
    
    return cell;
}

@end
