import FadingTableView

internal class ViewController: UIViewController {
    private let tableView: FadingTableView = {
        let view = FadingTableView(frame: .zero)
        return view
    }()
    internal override func loadView() {
        super.loadView()

        tableView.dataSource = self
        tableView.register(ImageCell.self, forCellReuseIdentifier: ImageCell.cellIdentifier)
        tableView.frame = view.bounds

        
        let fadingConfiguration = FadingTableViewConfiguration(opaqueFadeDistance: 5,
                                                               gradientFadeDistance: 30,
                                                               fadeColor: .white)
 
        tableView.topConfiguration = fadingConfiguration
        tableView.bottomConfiguration = fadingConfiguration
        tableView.animationDuration = 0.25

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140

        view.addSubview(tableView)
    }

    override internal func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        tableView.frame = CGRect(x: view.safeAreaInsets.left,
                                 y: view.safeAreaInsets.top,
                                 width: view.frame.width - view.safeAreaInsets.left - view.safeAreaInsets.right,
                                 height: view.frame.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom)
    }
}

extension ViewController: UITableViewDataSource {
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ImageCell.cellIdentifier) else {
            return UITableViewCell(style: .default, reuseIdentifier: ImageCell.cellIdentifier)
        }

        let image = UIImage(named: "\(indexPath.row + 1)")
        cell.imageView?.image = image

        return cell
    }
}
