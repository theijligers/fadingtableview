import Foundation

@objc
@objcMembers
public final class FadingTableViewConfiguration: NSObject {
    public let opaqueFadeDistance: CGFloat
    public let gradientFadeDistance: CGFloat
    public let fadeColor: UIColor

    public init(opaqueFadeDistance: CGFloat = 5, gradientFadeDistance: CGFloat = 30, fadeColor: UIColor = .green) {
        self.opaqueFadeDistance = opaqueFadeDistance
        self.gradientFadeDistance = gradientFadeDistance
        self.fadeColor = fadeColor
    }
}
