import UIKit

@objc
@objcMembers
public class FadingTableView: UITableView {

    private let gradientView: GradientView = {
        let view = GradientView(frame: .zero)
        view.isUserInteractionEnabled = false
        return view
    }()

    private var contentOffsetObservation: NSKeyValueObservation?

    public var topConfiguration: FadingTableViewConfiguration? {
        set {
            gradientView.topConfiguration = newValue
        }
        get {
            return gradientView.topConfiguration
        }
    }

    public var bottomConfiguration: FadingTableViewConfiguration? {
        set {
            gradientView.bottomConfiguration = newValue
        }
        get {
            return gradientView.bottomConfiguration
        }
    }

    public var animationDuration: Double {
        set {
            gradientView.animationDuration = newValue
        }
        get {
            return gradientView.animationDuration ?? 0
        }
    }

    override public init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)

        contentOffsetObservation = observe(\.contentOffset) { [weak self] _, _ in
            self?.layoutGradientView()
        }
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func layoutSubviews() {
        super.layoutSubviews()

        layoutGradientView()
    }

    private func addGradientViewIfNeeded() {
        if gradientView.superview == nil {
            gradientView.frame = frame
            
            superview?.addSubview(gradientView)
        }
    }

    private func layoutGradientView() {
        addGradientViewIfNeeded()

        gradientView.frame = frame
        
        gradientView.showTopLayer = contentOffset.y > 0        
        gradientView.showBottomLayer = contentOffset.y < contentSize.height - bounds.height
    }
}
