import UIKit

internal class GradientView: UIView {
    private let maskLayer = CAGradientLayer()

    override internal class var layerClass: AnyClass {
        return CAGradientLayer.self
    }

    internal var topConfiguration: FadingTableViewConfiguration? = FadingTableViewConfiguration() {
        didSet {
            setNeedsLayout()
        }
    }

    internal var bottomConfiguration: FadingTableViewConfiguration? = FadingTableViewConfiguration() {
        didSet {
            setNeedsLayout()
        }
    }

    internal var showTopLayer: Bool = false {
        didSet {
            guard showTopLayer != oldValue else {
                return
            }

            self.setNeedsLayout()
        }
    }

    internal var showBottomLayer: Bool = false {
        didSet {
            guard showBottomLayer != oldValue else {
                return
            }

            self.setNeedsLayout()
        }
    }

    internal var animationDuration: TimeInterval?

    private var topLayerLocations: [CGFloat] {
        guard let configuration = topConfiguration else {
            return []
        }

        return [0.0,
                configuration.opaqueFadeDistance / bounds.size.height,
                (configuration.opaqueFadeDistance + configuration.gradientFadeDistance) / bounds.size.height]
    }

    private var bottomLayerLocations: [CGFloat] {
        guard let configuration = bottomConfiguration else {
            return []
        }

        return [1.0 - ((configuration.opaqueFadeDistance + configuration.gradientFadeDistance) / bounds.size.height),
                1.0 - (configuration.opaqueFadeDistance / bounds.size.height), 1.0]
    }

    private var topColors: [CGColor] {
        guard let configuration = topConfiguration else {
            return []
        }

        let color = showTopLayer ? configuration.fadeColor : configuration.fadeColor.withAlphaComponent(0)

        return [color, color, configuration.fadeColor.withAlphaComponent(0)].map { $0.cgColor }
    }

    private var bottomColors: [CGColor] {
        guard let configuration = bottomConfiguration else {
            return []
        }

        let color = showBottomLayer ? configuration.fadeColor : configuration.fadeColor.withAlphaComponent(0)

        return [configuration.fadeColor.withAlphaComponent(0), color, color].map { $0.cgColor }
    }

    override internal func layoutSubviews() {
        super.layoutSubviews()

        guard let layer = layer as? CAGradientLayer else {
            return
        }

        layer.frame = layer.bounds

        let locations = topLayerLocations + bottomLayerLocations
        let sortedLocations = locations.sorted()

        guard locations == sortedLocations else {
            layer.locations = nil
            layer.colors = nil

            return
        }

        let convertedLocations = locations.map { NSNumber(value: Float($0)) }

        if layer.locations != convertedLocations {
            layer.locations = convertedLocations
        }

        animateColors(for: layer)
    }

    private func animateColors(for gradientLayer: CAGradientLayer) {
        let animation = CABasicAnimation(keyPath: "colors")
        animation.fromValue = gradientLayer.presentation()?.colors
        animation.toValue = topColors + bottomColors
        animation.duration = animationDuration ?? 0

        CATransaction.begin()
        CATransaction.setDisableActions(true)
        gradientLayer.colors = topColors + bottomColors
        CATransaction.commit()

        layer.add(animation, forKey: "animateColors")
    }
}
